package com.factorIT.service;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jdom2.Element;
import org.springframework.stereotype.Service;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;


@Service
public class CreateExcel {

    public ByteArrayInputStream buildExcelDocument(Element xml) throws IOException{

        XSSFWorkbook workbook = new XSSFWorkbook();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        buildPageEmpresas(workbook, xml).write(byteArrayOutputStream);

        return new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
    }

    public Workbook buildPageEmpresas(Workbook workbook, Element xml){

        Sheet sheet1 = workbook.createSheet("Empresas");
        Sheet sheet2 = workbook.createSheet("Movimientos");
        Row row1 = sheet1.createRow(0);

        row1.createCell(0).setCellValue("Nro Contrato");
        row1.createCell(1).setCellValue("CUIT");
        row1.createCell(2).setCellValue("DENOMINACION");
        row1.createCell(3).setCellValue("DOMICILIO");
        row1.createCell(4).setCellValue("CODIGO POSTAL");
        row1.createCell(5).setCellValue("PRODUCTOR");

        List<Element>empresas = xml.getChildren();
        for (int i=0; i<empresas.size() ; i++){

            Row row2 = sheet1.createRow(1+i);
            row2.createCell(0).setCellValue(empresas.get(i).getChildTextTrim("NroContrato"));
            row2.createCell(1).setCellValue(empresas.get(i).getChildTextTrim("CUIT"));
            row2.createCell(2).setCellValue(empresas.get(i).getChildTextTrim("Denominacion"));
            row2.createCell(3).setCellValue(empresas.get(i).getChildTextTrim("Domicilio"));
            row2.createCell(4).setCellValue(empresas.get(i).getChildTextTrim("CodigoPostal"));
            row2.createCell(5).setCellValue(empresas.get(i).getChildTextTrim("Productor"));

            buildPageMovimientos(sheet2, i, empresas.get(i).getChildTextTrim("NroContrato"), empresas.get(i).getChildren("Movimientos"));
        }
        return workbook;
    }

    public void buildPageMovimientos(Sheet sheet2, int i, String elem, List<Element>movimientos){
        Row row1 = sheet2.createRow(1+i);
        Row row2 = sheet2.createRow(0);
        row2.createCell(0).setCellValue("Nro Contrato");
        row2.createCell(1).setCellValue("SaldoCtaCte");
        row2.createCell(2).setCellValue("Concepto");
        row2.createCell(3).setCellValue("Importe");

        for(int j=0; j<movimientos.size(); j++ ){
            List<Element>movimiento = movimientos.get(j).getChildren("Movimiento");
            for(int k=0; k<movimiento.size(); k++ ){
                row1.createCell(k).setCellValue(elem);
                row1.createCell(1).setCellValue(movimiento.get(k).getChildTextTrim("SaldoCtaCte"));
                row1.createCell(2).setCellValue(movimiento.get(k).getChildTextTrim("Concepto"));
                row1.createCell(3).setCellValue(movimiento.get(k).getChildTextTrim("Importe"));
            }
        }
    }

}
