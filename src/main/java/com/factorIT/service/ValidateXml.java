package com.factorIT.service;

import com.factorIT.exception.InvalidFormXmlException;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.input.sax.XMLReaderJDOMFactory;
import org.jdom2.input.sax.XMLReaderXSDFactory;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

@Service
public class ValidateXml{

    public Element validateXML(InputStream xmlFile) throws JDOMException, IOException {
        Element xml= null;
        try {
            URL xsd = ValidateXml.class.getResource("/example.xsd");

            XMLReaderJDOMFactory factory = new XMLReaderXSDFactory(xsd);
            SAXBuilder builder = new SAXBuilder(factory);
            Document document = builder.build(xmlFile);

            xml = document.getRootElement();

        } catch (Exception ex) {
            String[] parts =ex.getMessage().split(":");
            String finalMessage=parts[2];

            throw new InvalidFormXmlException(finalMessage);
        }

        return xml;
    }
}