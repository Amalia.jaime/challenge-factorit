package com.factorIT.controller;

import com.factorIT.service.CreateExcel;
import com.factorIT.service.ValidateXml;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;


@RestController
public class DocumentController {

    @Autowired
    private ValidateXml validateXml;

    @Autowired
    private CreateExcel createExcel;

    @PostMapping("/export")
    public ResponseEntity<InputStreamResource> exportExcel(InputStream xmlFile) throws JDOMException, IOException, RuntimeException {

        Element xml = validateXml.validateXML(xmlFile);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=Libro1.xlsx");

        ByteArrayInputStream a = createExcel.buildExcelDocument(xml);

        return ResponseEntity.ok().headers(headers).body(new InputStreamResource(a));
    }

}
