package com.factorIT.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class InvalidFormXmlException extends RuntimeException {

    public InvalidFormXmlException(String message) {
        super(String.format(message));

    }

}
